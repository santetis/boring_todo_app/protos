///
//  Generated code. Do not modify.
//  source: todo.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

const CreateTodo$json = const {
  '1': 'CreateTodo',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
  ],
};

const UpdateTodo$json = const {
  '1': 'UpdateTodo',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'newTitle', '3': 2, '4': 1, '5': 9, '10': 'newTitle'},
    const {'1': 'newDescription', '3': 3, '4': 1, '5': 9, '10': 'newDescription'},
  ],
};

const DeleteTodo$json = const {
  '1': 'DeleteTodo',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
  ],
};

const Todo$json = const {
  '1': 'Todo',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'checked', '3': 4, '4': 1, '5': 8, '10': 'checked'},
    const {'1': 'users', '3': 5, '4': 3, '5': 9, '10': 'users'},
  ],
};

