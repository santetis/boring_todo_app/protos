///
//  Generated code. Do not modify.
//  source: account.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

import 'dart:async';

import 'package:grpc/grpc.dart';

import 'account.pb.dart';
import 'empty.pb.dart' as $0;
export 'account.pb.dart';

class AccountServiceClient extends Client {
  static final _$signIn = new ClientMethod<SignInRequest, SignInResponse>(
      '/boringtodoapp.AccountService/SignIn',
      (SignInRequest value) => value.writeToBuffer(),
      (List<int> value) => new SignInResponse.fromBuffer(value));
  static final _$signUp = new ClientMethod<SignUpRequest, $0.Empty>(
      '/boringtodoapp.AccountService/SignUp',
      (SignUpRequest value) => value.writeToBuffer(),
      (List<int> value) => new $0.Empty.fromBuffer(value));

  AccountServiceClient(ClientChannel channel, {CallOptions options})
      : super(channel, options: options);

  ResponseFuture<SignInResponse> signIn(SignInRequest request,
      {CallOptions options}) {
    final call = $createCall(_$signIn, new Stream.fromIterable([request]),
        options: options);
    return new ResponseFuture(call);
  }

  ResponseFuture<$0.Empty> signUp(SignUpRequest request,
      {CallOptions options}) {
    final call = $createCall(_$signUp, new Stream.fromIterable([request]),
        options: options);
    return new ResponseFuture(call);
  }
}

abstract class AccountServiceBase extends Service {
  String get $name => 'boringtodoapp.AccountService';

  AccountServiceBase() {
    $addMethod(new ServiceMethod<SignInRequest, SignInResponse>(
        'SignIn',
        signIn_Pre,
        false,
        false,
        (List<int> value) => new SignInRequest.fromBuffer(value),
        (SignInResponse value) => value.writeToBuffer()));
    $addMethod(new ServiceMethod<SignUpRequest, $0.Empty>(
        'SignUp',
        signUp_Pre,
        false,
        false,
        (List<int> value) => new SignUpRequest.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
  }

  Future<SignInResponse> signIn_Pre(ServiceCall call, Future request) async {
    return signIn(call, await request);
  }

  Future<$0.Empty> signUp_Pre(ServiceCall call, Future request) async {
    return signUp(call, await request);
  }

  Future<SignInResponse> signIn(ServiceCall call, SignInRequest request);
  Future<$0.Empty> signUp(ServiceCall call, SignUpRequest request);
}
