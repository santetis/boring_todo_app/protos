///
//  Generated code. Do not modify.
//  source: todo.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

import 'dart:async';

import 'package:grpc/grpc.dart';

import 'todo.pb.dart';
import 'empty.pb.dart' as $0;
export 'todo.pb.dart';

class TodoServiceClient extends Client {
  static final _$createTodo = new ClientMethod<CreateTodo, Todo>(
      '/boringtodoapp.TodoService/createTodo',
      (CreateTodo value) => value.writeToBuffer(),
      (List<int> value) => new Todo.fromBuffer(value));
  static final _$updateTodo = new ClientMethod<CreateTodo, $0.Empty>(
      '/boringtodoapp.TodoService/updateTodo',
      (CreateTodo value) => value.writeToBuffer(),
      (List<int> value) => new $0.Empty.fromBuffer(value));
  static final _$deleteTodo = new ClientMethod<CreateTodo, $0.Empty>(
      '/boringtodoapp.TodoService/deleteTodo',
      (CreateTodo value) => value.writeToBuffer(),
      (List<int> value) => new $0.Empty.fromBuffer(value));

  TodoServiceClient(ClientChannel channel, {CallOptions options})
      : super(channel, options: options);

  ResponseFuture<Todo> createTodo(CreateTodo request, {CallOptions options}) {
    final call = $createCall(_$createTodo, new Stream.fromIterable([request]),
        options: options);
    return new ResponseFuture(call);
  }

  ResponseFuture<$0.Empty> updateTodo(CreateTodo request,
      {CallOptions options}) {
    final call = $createCall(_$updateTodo, new Stream.fromIterable([request]),
        options: options);
    return new ResponseFuture(call);
  }

  ResponseFuture<$0.Empty> deleteTodo(CreateTodo request,
      {CallOptions options}) {
    final call = $createCall(_$deleteTodo, new Stream.fromIterable([request]),
        options: options);
    return new ResponseFuture(call);
  }
}

abstract class TodoServiceBase extends Service {
  String get $name => 'boringtodoapp.TodoService';

  TodoServiceBase() {
    $addMethod(new ServiceMethod<CreateTodo, Todo>(
        'createTodo',
        createTodo_Pre,
        false,
        false,
        (List<int> value) => new CreateTodo.fromBuffer(value),
        (Todo value) => value.writeToBuffer()));
    $addMethod(new ServiceMethod<CreateTodo, $0.Empty>(
        'updateTodo',
        updateTodo_Pre,
        false,
        false,
        (List<int> value) => new CreateTodo.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
    $addMethod(new ServiceMethod<CreateTodo, $0.Empty>(
        'deleteTodo',
        deleteTodo_Pre,
        false,
        false,
        (List<int> value) => new CreateTodo.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
  }

  Future<Todo> createTodo_Pre(ServiceCall call, Future request) async {
    return createTodo(call, await request);
  }

  Future<$0.Empty> updateTodo_Pre(ServiceCall call, Future request) async {
    return updateTodo(call, await request);
  }

  Future<$0.Empty> deleteTodo_Pre(ServiceCall call, Future request) async {
    return deleteTodo(call, await request);
  }

  Future<Todo> createTodo(ServiceCall call, CreateTodo request);
  Future<$0.Empty> updateTodo(ServiceCall call, CreateTodo request);
  Future<$0.Empty> deleteTodo(ServiceCall call, CreateTodo request);
}
