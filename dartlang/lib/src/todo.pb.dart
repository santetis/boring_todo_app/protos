///
//  Generated code. Do not modify.
//  source: todo.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

// ignore: UNUSED_SHOWN_NAME
import 'dart:core' show int, bool, double, String, List, override;

import 'package:protobuf/protobuf.dart' as $pb;

class CreateTodo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('CreateTodo', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'title')
    ..aOS(2, 'description')
    ..hasRequiredFields = false
  ;

  CreateTodo() : super();
  CreateTodo.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  CreateTodo.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  CreateTodo clone() => new CreateTodo()..mergeFromMessage(this);
  CreateTodo copyWith(void Function(CreateTodo) updates) => super.copyWith((message) => updates(message as CreateTodo));
  $pb.BuilderInfo get info_ => _i;
  static CreateTodo create() => new CreateTodo();
  static $pb.PbList<CreateTodo> createRepeated() => new $pb.PbList<CreateTodo>();
  static CreateTodo getDefault() => _defaultInstance ??= create()..freeze();
  static CreateTodo _defaultInstance;
  static void $checkItem(CreateTodo v) {
    if (v is! CreateTodo) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get title => $_getS(0, '');
  set title(String v) { $_setString(0, v); }
  bool hasTitle() => $_has(0);
  void clearTitle() => clearField(1);

  String get description => $_getS(1, '');
  set description(String v) { $_setString(1, v); }
  bool hasDescription() => $_has(1);
  void clearDescription() => clearField(2);
}

class UpdateTodo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('UpdateTodo', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'id')
    ..aOS(2, 'newTitle')
    ..aOS(3, 'newDescription')
    ..hasRequiredFields = false
  ;

  UpdateTodo() : super();
  UpdateTodo.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  UpdateTodo.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  UpdateTodo clone() => new UpdateTodo()..mergeFromMessage(this);
  UpdateTodo copyWith(void Function(UpdateTodo) updates) => super.copyWith((message) => updates(message as UpdateTodo));
  $pb.BuilderInfo get info_ => _i;
  static UpdateTodo create() => new UpdateTodo();
  static $pb.PbList<UpdateTodo> createRepeated() => new $pb.PbList<UpdateTodo>();
  static UpdateTodo getDefault() => _defaultInstance ??= create()..freeze();
  static UpdateTodo _defaultInstance;
  static void $checkItem(UpdateTodo v) {
    if (v is! UpdateTodo) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get id => $_getS(0, '');
  set id(String v) { $_setString(0, v); }
  bool hasId() => $_has(0);
  void clearId() => clearField(1);

  String get newTitle => $_getS(1, '');
  set newTitle(String v) { $_setString(1, v); }
  bool hasNewTitle() => $_has(1);
  void clearNewTitle() => clearField(2);

  String get newDescription => $_getS(2, '');
  set newDescription(String v) { $_setString(2, v); }
  bool hasNewDescription() => $_has(2);
  void clearNewDescription() => clearField(3);
}

class DeleteTodo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('DeleteTodo', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'id')
    ..hasRequiredFields = false
  ;

  DeleteTodo() : super();
  DeleteTodo.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  DeleteTodo.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  DeleteTodo clone() => new DeleteTodo()..mergeFromMessage(this);
  DeleteTodo copyWith(void Function(DeleteTodo) updates) => super.copyWith((message) => updates(message as DeleteTodo));
  $pb.BuilderInfo get info_ => _i;
  static DeleteTodo create() => new DeleteTodo();
  static $pb.PbList<DeleteTodo> createRepeated() => new $pb.PbList<DeleteTodo>();
  static DeleteTodo getDefault() => _defaultInstance ??= create()..freeze();
  static DeleteTodo _defaultInstance;
  static void $checkItem(DeleteTodo v) {
    if (v is! DeleteTodo) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get id => $_getS(0, '');
  set id(String v) { $_setString(0, v); }
  bool hasId() => $_has(0);
  void clearId() => clearField(1);
}

class Todo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Todo', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'id')
    ..aOS(2, 'title')
    ..aOS(3, 'description')
    ..aOB(4, 'checked')
    ..pPS(5, 'users')
    ..hasRequiredFields = false
  ;

  Todo() : super();
  Todo.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Todo.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Todo clone() => new Todo()..mergeFromMessage(this);
  Todo copyWith(void Function(Todo) updates) => super.copyWith((message) => updates(message as Todo));
  $pb.BuilderInfo get info_ => _i;
  static Todo create() => new Todo();
  static $pb.PbList<Todo> createRepeated() => new $pb.PbList<Todo>();
  static Todo getDefault() => _defaultInstance ??= create()..freeze();
  static Todo _defaultInstance;
  static void $checkItem(Todo v) {
    if (v is! Todo) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get id => $_getS(0, '');
  set id(String v) { $_setString(0, v); }
  bool hasId() => $_has(0);
  void clearId() => clearField(1);

  String get title => $_getS(1, '');
  set title(String v) { $_setString(1, v); }
  bool hasTitle() => $_has(1);
  void clearTitle() => clearField(2);

  String get description => $_getS(2, '');
  set description(String v) { $_setString(2, v); }
  bool hasDescription() => $_has(2);
  void clearDescription() => clearField(3);

  bool get checked => $_get(3, false);
  set checked(bool v) { $_setBool(3, v); }
  bool hasChecked() => $_has(3);
  void clearChecked() => clearField(4);

  List<String> get users => $_getList(4);
}

